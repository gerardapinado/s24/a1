db.users.find(
	{$or:
		[
			{"firstName": /^s/i},
			{"lastName": /^d/i}
		]
	},
	{
		"_id": 0,
		"firstName": 1,
		"lastName": 1
	}
)

db.users.find(
	{
		$and: [ {"firstName": {$regex: 'e'}}, {"age": {$lte:30}} ]
	}
)